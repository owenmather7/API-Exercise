#
# Build stage
#
FROM maven:3.5-jdk-8 AS build
COPY pom.xml /usr/src/app/pom.xml
#Maven dependencies are cached for rebuilds
RUN mvn -f /usr/src/app/pom.xml dependency:go-offline -B
COPY src /usr/src/app/src
#Multithreaded (re)build with maven
RUN mvn -f /usr/src/app/pom.xml clean package -T 1C

#
# Package App
#
#Light weight image used
FROM adoptopenjdk/openjdk8-openj9:alpine-slim
MAINTAINER Owen Mather<owenmather7@gmail.com>
#Set Default Buld Args

LABEL application=cstitanic

ARG PORT=5000
ARG CSTITANIC_DB_HOST=postgres:5432
ARG CSTITANIC_DB_USER=postgres
ARG CSTITANIC_DB_PASSWORD=example
ARG CSTITANIC_DB_NAME=postgres
ARG CSTITANIC_REDIS_HOST=redis
ARG CSTITANIC_REDIS_PORT=6379
ARG CSTITANIC_REDIS_PASSWORD=example
#Set Environment with default/overriden ars
ENV PORT=$PORT \
    CSTITANIC_DB_HOST=$CSTITANIC_DB_HOST \
    CSTITANIC_DB_USER=$CSTITANIC_DB_USER \
    CSTITANIC_DB_PASSWORD=$CSTITANIC_DB_PASSWORD \
    CSTITANIC_DB_NAME=$CSTITANIC_DB_NAME \
    CSTITANIC_REDIS_HOST=$CSTITANIC_REDIS_HOST \
    CSTITANIC_REDIS_PORT=$CSTITANIC_REDIS_PORT \
    CSTITANIC_REDIS_PASSWORD=$CSTITANIC_REDIS_PASSWORD \
    APP_ROOT=/usr/app

#Copy across built jar
COPY --from=build /usr/src/app/target/titanic-0.0.1-SNAPSHOT.jar ${APP_ROOT}/titanic.jar
#Create user/group
RUN addgroup -g 10001 cstitanic && adduser -u 10001 cstitanic -G cstitanic -D
USER 10001
EXPOSE $PORT
WORKDIR ${APP_ROOT}
ENTRYPOINT java -Djava.security.egd=file:/dev/./urandom -Xshareclasses -Xquickstart -jar titanic.jar
