##Makefile from https://blog.container-solutions.com/tagging-docker-images-the-right-way :)

NAME   := registry.gitlab.com/owenmather7/api-exercise/master
TAG    := $$(git log -1 --pretty=%!H(MISSING))
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest

build:
  @docker build -t ${IMG} .
  @docker tag ${IMG} ${LATEST}

push:
  @docker push ${NAME}

login:
  @docker login -u ${DOCKER_USER} -p ${DOCKER_PASS}