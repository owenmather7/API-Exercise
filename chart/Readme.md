#Helm Chart
This helm chart deploys the api-service application with redis and postgresql

This runs and is deployed to kubernetes with gitlabs Auto Devops CI/CD pipeline.
 
##Usage
Use the following command can be used when installing chart to use stable release.
`helm install --set image.version=stable ./chart`