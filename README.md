[![pipeline status](https://gitlab.com/owenmather7/API-Exercise/badges/master/pipeline.svg)](https://gitlab.com/owenmather7/API-Exercise/commits/master)
[![coverage report](https://gitlab.com/owenmather7/API-Exercise/badges/master/coverage.svg)](https://gitlab.com/owenmather7/API-Exercise/commits/master)
## Titanic API CS SRE Project
Uses spring-boot, redis + postgresql

Latest instance served on GKE with Gitlabs AutoDevops from this repo can be seen at https://api.owen.works/

Uses gitlabs AutoDevops Nginx intergration for Ingress and Helm for Auto chart deployment.

AutoDevops also supports CertManger for auto SSL with LetsEncrypt but this has not been implemented yet.

### Server
Spring-boot with embedded tomcat was chosen. Allows for quick templating for this project but is slow (boot time + response time) with large overhead
Spring data JPA abstracts persistence db interactions

### Frontend
Small frontend uses Thymeleaf to serve root /,/people and /people/{uuid} endpoint
No real time spent here. Uses inline CSS and no frontend build system

### Database
Postgres -> Uses `uuid-ossp` extension to handle uuid generation
This extension is added during db init. Database user must be superuser for this to be added

Application uses flywaydb to handle database initalization, schema versioning and upgrading

### Cache
Redis cache is used to improve performance
Currently this must be present and must have a password. 
Future work would support graceful failure for this service

# Usage
## Docker Compose
[docker-compose-debug.yml](./docker-compose-debug.yml) can be used to build the image from source

```
#Build image
docker-compose -f docker-compose-debug.yml build

#Start container
docker-compose -f docker-compose-debug.yml up

#Build and start image
docker-compose -f docker-compose-debug.yml up --build
```

[docker-compose-mon.yml](./docker-compose-mon.yml) This compose image creates a monitoring environment using prometheus and grafana
```
docker-compose -f docker-compose-mon.yml up
```
Springs /acutator endpoints are scraped with prometheus 
A basic dashboad is served on grafana on localhost:3000 (admin/admin) -> settings can be changed in [grafana.ini](deploy/grafana/config/grafana.ini)

[docker-compose.yml](./docker-compose.yml) This compose image deploys both api deployment and monitoring environment 
This image does not build from source. Docker credentials must be predefined to pull form gitlab registry
```
docker-compose up -d
```

## Environment Variables
The api-service docker image respects the following environment variables. Update them as appropriate in your deployment
```yml
CSTITANIC_DB_HOST=hostname:port
CSTITANIC_DB_USER=dbUser
CSTITANIC_DB_PASSWORD=dbPassword
CSTITANIC_DB_NAME=dbName
CSTITANIC_REDIS_HOST=redis
CSTITANIC_REDIS_PORT=6379
CSTITANIC_REDIS_PASSWORD=password
```

## Kubernetes
To create a Kubernetes deployment run the following command:
```bash
kubectl apply -f k8s
```

A secret called regred must be predefined in the environment to pull from the private repo!!
[Pull from private repo](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)

## Helm 
Kubernetes deployment can also be done using helm chart. This is how it is currently rolled out to our production environment using gitlabs AutoDevops 

You can deploy with helm by pulling this repo and running:
`helm install production ./chart`

You must have a registry secret predefined to pull the api-service image!. The deployment looks for a secret named `gitlab-registry` but this can be changed like below:
`helm install --set imagePullSecrets=myRegSecret ./chart`

Our CI/CD build overrides variables like `dbPassword` when deploying by setting `HELM_UPGRADE_EXTRA_ARGS` gitlab environment variable like below:
```bash
--values chart/values.yaml --set image.version=$CI_COMMIT_SHA --set dbPassword=$mySecretPassword --set redisPassword=$mySecretPassword
```

For a list of overrideable properties see [values.yaml](chart/values.yaml)

## Notes
Whats important to a SRE?
- Reducing toil 
    - Tasks are automated as often as possible
    - Rollback + Autoscale
    - CI/CD
- Capacity Planning + Determining SLIs and key metrics
    - What qualifies as unusual behaviour?
- Servers/Services are meeting and tracking SLOs
    - Prediction model? Will we break SLO down the line at current performance?
- Feedback/Alerting solid coverage with minimal interrupts (tickets>alerts)

## Capacity Planning / SLO Calculation
What are the minimal/maximum requirements for this application? How will it scale? How does it perform under load?
#### Resource Usage
From `docker stats"`:
Initally rolled out with gcr.io/distroless/java showed 300+MiB usage:
![Kiku](images/dockerStats1.PNG)

Switching to openjdk reduced footprint significantly - adoptopenjdk/openjdk8-openj9:alpine-slim:
![Kiku](images/dockerStats2.PNG)

~100MiB for cstitanic application
Still quite large but okay for this test

#### Load Tests
Tested with [wrk](https://github.com/wg/wrk) to simulate load.
99% percentile at 356ms latency under 100 req/s load
![Kiku](images/Latency.PNG)

With redis cache 99% percentile down to 100ms latency under 100 req/s load
![Kiku](images/Cached.PNG)

Not great performance but we are using spring-boot as it is quick to template and for this project the results are negligible

## Monitoring
One Prom per cluster 2 per global aggregator

Attempt to capture 4 Golden Signals for Monitoring:
Latency, Errors, Traffic, Saturation
### docker-compose
Captured By Prometheus and vizualized with Grafana
- Spring boot actuator + micrometer jar exposes enhanced http metrics in prometheus readable format
- Node Exporter produces host metrics
- Postgres Exporter - exports DB metrics in prometheus readable format

### Helm
Uses gitlabs prometheus with Auto Devops to scrape metrics

## TODO/Improvements:
- Centralized log collector and data analysis
- Integration with Alerts/Ticketing System
    - +Predictive analysis ~Issue may occur in next X hours eg: Storage/Any other resource usage scaling linearly
- Service Discovery
- Security related improvements (encryption,secrets,networking,auth,audit,ACLs)
- Templating
- Application Performance Improvements - Pagination, Change Application Server, HTTP2, Tuning, Reduce, Graceful Failure (cache unavailable etc)
- Clean up frontend (remove inline css, minification, etc)
- No persistence currently enabled on Postgres. Can set property in helm to enable if required.