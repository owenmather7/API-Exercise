package com.owenmather.titanic.api.service;

import java.util.List;
import java.util.UUID;

import com.owenmather.titanic.api.entity.Person;

public interface PersonService {

    public List<Person> findAll();

    public Person findByUuid(UUID theId);

    public Person save(Person thPerson);

    public void deleteByUuid(UUID theId);

    public Person updatePerson(Person thPerson, UUID uuid);
}
