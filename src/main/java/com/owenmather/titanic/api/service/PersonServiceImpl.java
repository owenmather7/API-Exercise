package com.owenmather.titanic.api.service;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.owenmather.titanic.api.dao.PersonRepository;
import com.owenmather.titanic.api.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    private PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository thePersonRepository) {
        personRepository = thePersonRepository;
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    @Cacheable(value="people",key = "#theId")
    public Person findByUuid(UUID theId) {
        Optional<Person> result = personRepository.findById(theId);
        if (!result.isPresent()) {
            return null;
        }
        return result.get();
    }

    @Override
    public Person save(Person thePerson) {
        return personRepository.save(thePerson);
    }

    @Override
    @CacheEvict(value="people",key = "#theId")
    public void deleteByUuid(UUID theId) {
        personRepository.deleteById(theId);
    }

    @Override
    @CacheEvict(value="people",key = "#uuid")
    public Person updatePerson(Person thePerson, UUID uuid) {
        thePerson.setUuid(uuid);
        //Using predefined uuid allows save to work as update
        return personRepository.save(thePerson);
    }

}






