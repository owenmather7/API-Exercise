package com.owenmather.titanic.api.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import com.owenmather.titanic.api.entity.Person;
import com.owenmather.titanic.api.service.PersonService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@Controller
@RequestMapping("/people")
public class PersonController {
    private PersonService personService;

    public PersonController(PersonService thePersonService) {
        personService = thePersonService;
    }

    @ResponseBody
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Person>> listPeople() {
        List<Person> retPersons = personService.findAll();
        if (retPersons == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(retPersons);
    }

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String listPeoplePage(Model model) {
        List<Person> retPersons = personService.findAll();
        model.addAttribute("people",retPersons);
        return "peopleList.html";
    }

    @ResponseBody
    @GetMapping(value = "/{uuid}",produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> getPerson(@PathVariable("uuid") UUID uuid) {
        Person retPerson =  personService.findByUuid(uuid);
        if (retPerson == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(retPerson);
    }

    @GetMapping(value = "/{uuid}",produces=MediaType.TEXT_HTML_VALUE)
    public String getPersonPage(@PathVariable("uuid") UUID uuid, Model model) {
        Person retPerson =  personService.findByUuid(uuid);
        model.addAttribute("person",retPerson);
        return "person.html";
    }

    @ResponseBody
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> createPerson(@RequestBody Person thePerson) throws URISyntaxException {
        Person createdPerson = personService.save(thePerson);
        if (createdPerson == null) {
            return ResponseEntity.notFound().build();
        }

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(createdPerson.getUuid())
                .toUri();
        return ResponseEntity.created(uri)
                .body(createdPerson);

    }

    @ResponseBody
    @PutMapping(value = "/{uuid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> updatePerson(@RequestBody Person person, @PathVariable("uuid") UUID uuid) {
        Person updatedPerson = personService.updatePerson(person, uuid);
        if (updatedPerson == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedPerson);

    }

    @ResponseBody
    @DeleteMapping(value = "/{uuid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deletePerson(@PathVariable("uuid") UUID uuid) {
        //verify person exists
        if(personService.findByUuid(uuid)==null){
            return ResponseEntity.notFound().build();
        }
        //Person found deleting
        personService.deleteByUuid(uuid);
        return ResponseEntity.noContent().build();
    }
}












