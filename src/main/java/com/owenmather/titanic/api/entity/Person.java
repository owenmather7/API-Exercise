package com.owenmather.titanic.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.owenmather.titanic.api.entity.PersonData;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * Person
 */
@Entity
@Table(name="Person")
public class Person extends PersonData implements Serializable {

  private static final long serialVersionUID=1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonProperty("uuid")
  private UUID uuid;

  public Person(){
  }

  public Person uuid(UUID uuid) {
    this.uuid = uuid;
    return this;
  }

  /**
   * Get uuid
   * @return uuid
  */
  @ApiModelProperty(value = "")

  @Valid

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Person person = (Person) o;
    return Objects.equals(this.uuid, person.uuid) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Person {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    uuid: ").append(toIndentedString(uuid)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

