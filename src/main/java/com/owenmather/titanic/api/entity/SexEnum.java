package com.owenmather.titanic.api.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * Gets or Sets sex
 */
public enum SexEnum implements Serializable {
    male("male"),

    female("female"),

    other("other");

    private String value;

    SexEnum(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static SexEnum fromValue(String text) {
        for (SexEnum b : SexEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
}