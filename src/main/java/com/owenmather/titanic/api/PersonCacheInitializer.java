package com.owenmather.titanic.api;

import com.owenmather.titanic.api.entity.Person;
import com.owenmather.titanic.api.service.PersonService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

//Initialize cache with data on boot
@Service
public class PersonCacheInitializer {
    @Autowired
    PersonService personService;
    @Autowired
    CacheManager cacheManager;

    private static final Logger LOG = Logger.getLogger(PersonCacheInitializer.class);

    @PostConstruct
    public void init() {
        update();
    }

    public void update() {
        for (Person person : personService.findAll()) {
            personService.findByUuid(person.getUuid());
        }
        LOG.info("Cache initialized");
    }
}