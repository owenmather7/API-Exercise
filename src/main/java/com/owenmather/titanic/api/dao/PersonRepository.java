package com.owenmather.titanic.api.dao;

import com.owenmather.titanic.api.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Transactional
public interface PersonRepository extends JpaRepository<Person, UUID> {

}
